import {v4 as uuid4} from 'uuid';

type Subscriber = (count: number) => void;

export class Counter {
    private count: number = 0;
    private subscribers = new Map<string, Subscriber>();

    getCount(): number {
        return this.count;
    }

    setCount(value: number) {
        this.count = value;
        this.subscribers.forEach((subscriber) => subscriber(value));
    }

    subscribe(subscriber: Subscriber) {
        const id = uuid4();
        this.subscribers.set(id, subscriber);

        return () => {
this.subscribers.delete(id);
        }
    }
}