import './style.css'
import typescriptLogo from './typescript.svg'
import viteLogo from '/vite.svg'
import {CounterComponent} from './counterComponent.ts'
import {SumComponent} from "./sumComponent.ts";
import {Counter} from "./counter.ts";

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <a href="https://www.typescriptlang.org/" target="_blank">
      <img src="${typescriptLogo}" class="logo vanilla" alt="TypeScript logo" />
    </a>
    <h1>Vite + TypeScript</h1>
    <div class="card">
      <button id="counter1" type="button"></button>
      <button id="counter2" type="button"></button>
      <span id="sum"></span>
    </div>
    <p class="read-the-docs">
      Click on the Vite and TypeScript logos to learn more
    </p>
  </div>
`

const counter1 = new Counter();
const counter2 = new Counter();

// setupCounter(document.querySelector<HTMLButtonElement>('#counter')!)
new CounterComponent(document.querySelector<HTMLButtonElement>('#counter1')!, counter1);
new CounterComponent(document.querySelector<HTMLButtonElement>('#counter2')!, counter2);
new SumComponent(document.querySelector<HTMLSpanElement>('#sum')!, [counter1, counter2]);
