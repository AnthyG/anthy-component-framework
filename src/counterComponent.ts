import {Counter} from "./counter.ts";

export function setupCounter(element: HTMLButtonElement) {
  let counter = 0
  const setCounter = (count: number) => {
    counter = count
    element.innerHTML = `count is ${counter}`
  }
  element.addEventListener('click', () => setCounter(counter + 1))
  setCounter(0)
}

export class CounterComponent {
  private element: HTMLButtonElement;
  private counter: Counter;
  private counterUnsubscribe: Function;

  constructor(element: HTMLButtonElement, counter: Counter) {
    this.element = element;
    this.counter = counter;

    this.subscribeToCounter();
    this.registerEventListener();

    this.render();
  }

  private subscribeToCounter() {
    this.counterUnsubscribe = this.counter.subscribe(() => {
      this.render();
    })
  }

  private registerEventListener() {
    this.element.addEventListener('click', () => this.counter.setCount(this.counter.getCount() + 1))
  }

  public render() {
    this.element.innerHTML = `count is ${this.counter.getCount()}`;
  }

  public destroy() {
    this.counterUnsubscribe();
  }
}
