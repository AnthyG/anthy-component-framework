import {Counter} from "./counter.ts";

export class SumComponent {
    private element: HTMLElement;
    private counters: Counter[];
    private counterUnsubscribes: Function[];

    constructor(element: HTMLElement, counters: Counter[] = []) {
        this.element = element;
        this.counters = counters;

        this.subscribeToCounters();

        this.render();
    }

    private subscribeToCounters() {
        this.counterUnsubscribes = this.counters.map(counter=>counter.subscribe(() => {
            this.render();
        }))
    }

    private calculateSum() {
        return this.counters.reduce((sum,counter)=>sum+counter.getCount(),0);
    }

    public render() {
        this.element.innerHTML = `sum is ${this.calculateSum()}`;
    }

    public destroy() {
        this.counterUnsubscribes.forEach(unsubscribe=>unsubscribe());
    }
}